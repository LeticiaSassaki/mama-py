import cv2
import numpy as np
from matplotlib import pyplot as plt

#Entrada
imgname = "images/mama6jpeg"
imgOriginal = cv2.imread(imgname)
img = cv2.imread(imgname)

# Pre-processamento com filtros
kernel = np.ones((3, 3), np.uint8)
morphology = cv2.morphologyEx(img, cv2.MORPH_OPEN, kernel, iterations=5)

#Segmentacao
threshold = cv2.threshold(morphology, 127, 255, cv2.THRESH_BINARY)[1]

#Pos-processamento
threshold_rgb_2_gray = cv2.cvtColor(threshold, cv2.COLOR_RGB2GRAY)

#Reconhecimento
contours, hierarchy = cv2.findContours(threshold_rgb_2_gray, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

cv2.drawContours(img, contours, -1, (255, 0, 0), 3)

#Printing
titles = ['Original', 'morphology', 'threshold', 'threshold_rgb_2_gray', 'Result']
images = [imgOriginal, morphology, threshold, threshold_rgb_2_gray, img]


for i in xrange(images.__len__()):
    plt.subplot(2, 3, i+1), plt.imshow(images[i])
    plt.title(titles[i])
    plt.xticks([]), plt.yticks([])

plt.show()
